<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WZSistemas\CobrancaBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of DependenteRepository
 *
 */
class DependenteRepository extends EntityRepository
{

    public function getDepemdentesSemCartao()
    {
        $query = $this->createQueryBuilder("D");
        
        $query->select("D")
            ->leftJoin("D.cartoes", "CR")
            ->leftJoin("D.cliente", "C")
            ->andWhere($query->expr()->isNull("CR.id"));
        
        return $query->getQuery()->getResult();
    }

    
    
    
}
