<?php

namespace WZSistemas\CobrancaBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;


/**
 * Description of ClienteRepository
 *
 * @author Luciano
 */
class ClienteRepository extends EntityRepository
{
    
    public function getAniversariantes($mes) {
        $query = $this->createQueryBuilder("C");
        
        $query->select("C")
                ->andWhere("Month(C.nascimento) = :mes");
        $query->setParameter("mes", $mes);
        return $query->getQuery()->getResult();
    }
    
    public function getClientesSemCartao()
    {
        $query = $this->createQueryBuilder("C");
        
        $query->select("C")
            ->leftJoin("C.cartoes", "CR")
            ->leftJoin("C.dependentes", "D")
            ->andWhere($query->expr()->isNull("CR.id"));
        
        return $query->getQuery()->getResult();
    }
    /**
     * 
     * @param string $busca
     * @param int $maxResults
     * @param int $firstResult
     * @return type
     */
    public function getClientes($busca, $maxResults, $firstResult, $ordem = array())
    {
        $query = $this->createQueryBuilder("C");
        
        $query->select("C");
        if (!empty($busca)) {
            $query->andWhere($query->orWhere($query->expr()->like("C.nome", ":busca"))
                                    ->orWhere($query->expr()->like("C.cpf", ":busca"))
                                    ->orWhere($query->expr()->like("C.telefone", ":busca"))
                                    ->orWhere($query->expr()->like("C.telefone1", ":busca"))
                                    ->orWhere($query->expr()->like("C.telefone2", ":busca"))
                                    ->orWhere($query->expr()->like("C.cidade", ":busca"))
                                    ->getDQLPart("where"));
            $query->setParameter("busca", "%{$busca}%");
        }
        
        if (is_array($ordem)) {
            if ($ordem[0]['column'] == 0) {
                $order = "C.nome";
            } elseif ($ordem[0]['column'] == 1) {
                $order = "C.telefone";
            } elseif ($ordem[0]['column'] == 2) {
                $order = "C.cidade";
            }
            if ($ordem[0]['dir'] == "asc") {
                $dir = "ASC";
            } elseif ($ordem[0]['dir'] == "desc") {
                $dir = "DESC";
            }
            
            $query->orderBy($order, $dir);
        }
        
        if (($maxResults+$firstResult)>0) {
            $query->setFirstResult($firstResult)
                    ->setMaxResults($maxResults);
        }
                
        return $query->getQuery()->getResult();
    }
    
    /**
     * 
     * @return array
     */
    public function count($busca = "")
    {
        $query = $this->createQueryBuilder("C");
        
        $query->select("COUNT(C.id)");
        
        if (!empty($busca)) {
            $query->andWhere($query->orWhere($query->expr()->like("C.nome", ":busca"))
                                    ->orWhere($query->expr()->like("C.cpf", ":busca"))
                                    ->orWhere($query->expr()->like("C.cidade", ":busca"))
                                    ->getDQLPart("where"));
            $query->setParameter("busca", "%{$busca}%");
        }
        
        return $query->getQuery()->getSingleScalarResult();
    }
    
    
    /**
     * @param string $param
     * @return array
     */
    public function uniqueEntity($param) 
    {
        if (isset ($param['cpf'])) {
            if (empty($param['cpf'])) {
                return array();;
            } else {
                return $this->findBy($param);
            }
        }
        return array();;
    }
    
    /**
     * @param string $param
     * @return array
     */
    public function findClienteByCPF($param)
    {
        $query = $this->createQueryBuilder("C");
        
        $query->where("C.cpf = :param")
            ->setParameter("param", $param);
        
        try {
            return $query->getQuery()->getSingleResult();
        } catch (\Exception $exc) {
            return NULL;
        }
    }

    /**
     * 
     * @return int
     */
    public function getParcelasVencidas(\WZSistemas\CobrancaBundle\Entity\Cliente $cliente)
    {
        $query = $this->createQueryBuilder("C");
        
        $data = date("Y-m-d");
        
        $query->select($query->expr()->count("P.id"))
                ->leftJoin("C.dividas", "D")
                ->leftJoin("D.negociacoes", "N")
                ->leftJoin("N.parcelas", "P")
                ->andWhere($query->expr()->lt("P.vencimento", ":data"))
                ->andWhere($query->expr()->eq("P.pago", "0"))
                ->andWhere($query->expr()->eq("C.id", ":id"));
        $query->setParameter("data", $data, \PDO::PARAM_STR);
        $query->setParameter("id", $cliente->getId(), \PDO::PARAM_INT);
        return $query->getQuery()->getSingleScalarResult();        
    }


    
}
