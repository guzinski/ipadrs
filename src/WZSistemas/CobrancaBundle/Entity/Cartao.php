<?php

namespace WZSistemas\CobrancaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Cartão
 *
 * @ORM\Table(name="cartao")
 * @ORM\Entity(repositoryClass="WZSistemas\CobrancaBundle\Entity\Repository\CartaoRepository")
 */
class Cartao
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
        
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $lote;
    
    /**
     * @var \WZSistemas\CobrancaBundle\Entity\Cliente
     *
     * @ORM\ManyToOne(targetEntity="WZSistemas\CobrancaBundle\Entity\Cliente", inversedBy="cartoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cliente", referencedColumnName="id")
     * })
     */
    private $cliente;

    
    /**
     * @var \WZSistemas\CobrancaBundle\Entity\Dependente
     *
     * @ORM\ManyToOne(targetEntity="WZSistemas\CobrancaBundle\Entity\Dependente", inversedBy="cartoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dependente", referencedColumnName="id")
     * })
     */
    private $dependente;
    
    
    public function __construct($lote = null, Cliente $cliente = null, \WZSistemas\CobrancaBundle\Entity\Dependente $dependente = null)
    {
        if (!is_null($lote)) {
            $this->setLote($lote);
        }
        if (!is_null($cliente)) {
            $this->setCliente($cliente);
            if (!is_null($dependente)) {
                $this->setDependente($dependente);
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLote()
    {
        return $this->lote;
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function getDependente()
    {
        return $this->dependente;
    }
    
    public function setLote($lote)
    {
        $this->lote = $lote;
        return $this;
    }

    public function setCliente(\WZSistemas\CobrancaBundle\Entity\Cliente $cliente)
    {
        $this->cliente = $cliente;
        return $this;
    }

    public function setDependente(\WZSistemas\CobrancaBundle\Entity\Dependente $dependente)
    {
        $this->dependente = $dependente;
        return $this;
    }


    
    
    
    
    
}
