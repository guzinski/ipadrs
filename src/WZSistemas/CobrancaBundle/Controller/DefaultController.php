<?php

namespace WZSistemas\CobrancaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WZSistemas\CobrancaBundle\Entity\Cartao;
use WZSistemas\CobrancaBundle\Entity\Cliente;
use WZSistemas\CobrancaBundle\Entity\Registro;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Exception\InvalidArgumentException;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
    
    /**
     * @Route("/", name="_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * @Route("/parcelas", name="parcelas_vencidas")
     * @Template()
     */
    public function parcelasAction()
    {
        return array();
    }

    /**
     * @Route("/parcelas/pagination", name="_carrega_parcelas")
     * @return Response
     */
    public function paginationAction()
    {
        $rep = $this->getDoctrine()->getRepository("WZSistemas\CobrancaBundle\Entity\Parcela");
        $parcelas = $rep->getParcelasVencidas();
        $dados = [];
        foreach ($parcelas as $parcela) {
            $link = $this->generateUrl("_cobranca", array("cpf"=>$parcela->getNegociacao()->getDivida()->getCliente()->getCpf()));
            $dados[] = [
                "<a href=\"".$link."\">". $parcela->getNegociacao()->getDivida()->getCliente()->getNome()."</a>",
                "R$ ".number_format($parcela->getValor(), 2, ",", "."),
                $parcela->getVencimento()->format("d/m/Y")
            ];
        }
        $return['data'] = $dados;
        $return['recordsTotal'] = count($parcelas);
        $return['recordsFiltered'] = count($parcelas);
        return new Response(json_encode($return));
    }

    /**
     * @Route("/login", name="_login")
     * @Template()
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $errorMsg = "";
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->set(SecurityContext::AUTHENTICATION_ERROR, "");
        }
        
        if (is_object($error)) {
            $errorMsg = $this->get("translator")->trans($error->getMessage());
        }

        return array(
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $errorMsg,
        );
    }
    
    /**
     * @Route("/sem/permissao", name="_sem_permissao")
     * @Template()
     */
    public function semPermissaoAction()
    {
        return array();
    }
    
    /**
     * Área de acesso das Convenidas, para consutla de cliente
     * 
     * @Route("consulta/cliente", name="consulta_conveniada")
     * @Template()
     */
    public function consultaAction()
    {
        return array();
    }
    
    
    /**
     * Área de acesso das Convenidas, para consutla de cliente
     * 
     * @Route("consulta/cartao", name="consultar_cartao")
     */
    public function consutlarCartaoAction(Request $request)
    {
        $numero = $request->get("numero");
        $retorno = array();
        $retorno['result'] = FALSE;
        if (is_null($numero)) {
            $retorno['msg'] = "Insira o número do cartão";
        } else {
            $em = $this->getDoctrine()->getManager();
            $cartao = $em->find(Cartao::class, $numero);
            if (is_null($cartao)) {
                $retorno['msg'] = "Cartão inválido";
            } else {
                $parcelasVencidas = $em->getRepository(Cliente::class)->getPArcelasVencidas($cartao->getCliente());
                if ($parcelasVencidas>0) {
                    $retorno['msg'] = "Solicite o comparecimento ao WZSistemas dos Pescadores para maiores esclarecimentos";
                } else {
                    $retorno['result'] = TRUE;
                    $retorno['html'] = $this->renderView("WZSistemasCobrancaBundle::Default/htmlCliente.html.twig", array("cliente"=>$cartao->getCliente()));
                }
            }
            
        }
        return new Response(json_encode($retorno));
    }

    
    /**
     * @Route("/consulta/inserir/registro", name="inserir_registro_conveniada")
     * 
     * @param Request $request
     * @return Response
     */
    public function inseriRegistroAction(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $idCliente  = $request->request->getInt("id");
        $texto      = $request->get("texto");
        
        if (is_null($idCliente) || is_null($texto)) {
            throw new InvalidArgumentException;
        }
        
        $cliente    = $this->getDoctrine()->getRepository("WZSistemas\CobrancaBundle\Entity\Cliente")->find($idCliente);
        
        if (is_null($cliente)) {
            return new Response("Não foi possível registrar o histórico.");
        }
        
        $novoRegistro = new Registro();
        $novoRegistro->setTexto($this->getUser()->getNomeFantasia(). " - ". trim($texto));        
        $novoRegistro->setCliente($cliente);
        $em->persist($novoRegistro);
        $em->flush();
        
        
        return new Response("Histórico registrado com sucesso");
    }

    
    
}
