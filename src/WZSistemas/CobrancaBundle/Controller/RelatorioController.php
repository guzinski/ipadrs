<?php

namespace WZSistemas\CobrancaBundle\Controller;

use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Description of RelatorioController
 * @Route("/relatorios")
 * @author Luciano
 */
class RelatorioController extends Controller
{
    
    /**
     * 
     * @Route("/", name="_relatorios")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    /**
     * 
     * @Route("/html/form/aniversario", name="html_form_aniversario")
     * @Template()
     */
    public function htmlFormAniversarioAction()
    {
        return array();
    }
        
    /**
     * 
     * @Route("/html/form/cobranca", name="html_form_cobranca")
     * @Template()
     */
    public function htmlFormCobrancaAction()
    {
        return [];
    }
    
    /**
     * 
     * @Route("/aniversariantes/{mes}", name="relatorio_aniversariantes")
     * @Template()
     * @param int $idLoja
     * @param int $mes
     * @param int $ano
     */
    public function aniversariantesAction($mes = null)
    {
        if (is_null($mes) || !is_numeric($mes) || $mes<1 || $mes>12) {
            throw new NotFoundHttpException;
        }
        
        $clientes = $this->getDoctrine()->getRepository("WZSistemas\CobrancaBundle\Entity\Cliente")->getAniversariantes($mes);
                
        return array(
                "mes"=>$mes,
                "clientes"=>$clientes,
            );
    }
    
    /**
     * @Route("/inadinplentes", name="relatorio_inadinplentes")
     * @Template()
     * @return array
     */
    public function inadinplentesAction()
    {
        $parcelas = $this->getDoctrine()->getRepository("WZSistemas\CobrancaBundle\Entity\Parcela")->getParcelasVencidas();
        return array("parcelas"=>$parcelas);
    }
    
    /**
     * @Route("/cobranca", name="relatorio_cobranca")
     * @Template()
     * @param int $mes
     * @param int $ano
     * @return array
     */
    public function cobrancaAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $inicio = DateTime::createFromFormat("d/m/Y", $request->get("inicio"));
        $fim = DateTime::createFromFormat("d/m/Y", $request->get("fim"));
        
        $repository = $this->getDoctrine()->getRepository("WZSistemas\CobrancaBundle\Entity\Parcela");
        
        
        
        return array("parcelas"=>$repository->getParcelasPagas($inicio, $fim));
    }
    
    
    function valorPorExtenso($valor = 0, $complemento = true)
    {
        $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
        $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");

        $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
        $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
        $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
        $u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");

        $z = 0;

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for ($i = 0; $i < count($inteiro); $i++) {
            for ($ii = strlen($inteiro[$i]); $ii < 3; $ii++) {
                $inteiro[$i] = "0" . $inteiro[$i];
            }
        }

        // $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
        $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
        $rt = "";
        for ($i = 0; $i < count($inteiro); $i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";
            $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd && $ru) ? " e " : "") . $ru;
            $t = count($inteiro) - 1 - $i;
            if ($complemento == true) {
                $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
                if ($valor == "000") {
                    $z++;
                } elseif ($z > 0) {
                    $z--;
                }
                
                if (($t == 1) && ($z > 0) && ($inteiro[0] > 0)) {
                    $r .= (($z > 1) ? " de " : "") . $plural[$t];
                }
            }
            if ($r) {
                $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
            }
        }

        return($rt ? trim($rt) : "zero");
    }
}
