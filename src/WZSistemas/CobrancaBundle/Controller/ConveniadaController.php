<?php

namespace WZSistemas\CobrancaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WZSistemas\CobrancaBundle\Entity\Conveniada;
use WZSistemas\CobrancaBundle\Form\ConveniadaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of ConveniadaController
 * @Route("/conveniada")
 * @author Luciano
 */
class ConveniadaController extends Controller
{
    
    /**
     * @Route("/", name="conveniada_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    
    
    /**
     * @Route("/pagination", name="conveniada_pagination")
     * @return Response
     */
    public function paginationAction()
    {
        $conveniadas = $this->getDoctrine()->getRepository("WZSistemasCobrancaBundle:Conveniada")->findAll();
        $dados = [];
        foreach ($conveniadas as $conveniada) {
            $linha = array();
            $dados[] = [
                "<a href=\"".$this->generateUrl("conveniada_form", array("id"=>$conveniada->getId())) ."\">". $conveniada->getRazaoSocial() ."</a>",
                $conveniada->getResponsavel(),
                $conveniada->getTelefone(),
                "<a href=\"javascript:excluirConveniada(".$conveniada->getId() .");\"><i class=\"glyphicon glyphicon-trash\"></a>",
            ];
        }
        $return['recordsTotal'] = count($conveniadas);
        $return['recordsFiltered'] = count($conveniadas);
        $return['data'] = $dados;
        return new Response(json_encode($return));
    }

    
    /**
     * 
     * @Route("/editar/{id}", name="conveniada_form")
     * @Template()
     */
    public function formAction(Request $request, $id = 0) 
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($id>0) {
            $conveniada = $em->find("WZSistemasCobrancaBundle:Conveniada", $id);
        } else {
            $conveniada = new Conveniada();
            $senha = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"),0,6);
            $conveniada->setSenha($senha);
        }
        $form = $this->createForm(new ConveniadaType(), $conveniada);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($conveniada);
            $em->flush();
            return new RedirectResponse($this->generateUrl('conveniada_index'));
        }
        
        return array("conveniada"=>$conveniada, "form"=>$form->createView());
    }

    /**
     * @Route("/excluir", name="conveniada_excluir")
     */
    public function excluiUsuarioAction(Request $resquest) 
    {
        $respone = array();
        $id = $resquest->request->getInt("id", null);
        if (null != $id) {
            $em = $this->getDoctrine()->getManager();
            $conveniada = $em->find("WZSistemasCobrancaBundle:Conveniada", $id);
            $em->remove($conveniada);
            $em->flush();
            $respone['ok'] = 1;
        } else {
            $respone['ok'] = 0;
            $respone['error'] = "Erro ao exclui conveniada";
        }
        return new Response(json_encode($respone));
    }
    
    /**
     * 
     * @Route("/contrato", name="conveniada_contrato")
     * @return type
     */
    public function contratoAction()
    {
        return $this->get("upload")->download($this->get('kernel')->getRootDir() . '/../web/bundles/wzsistemascobranca/contrato/conveniada_contrato.docx');
    }

    
}
