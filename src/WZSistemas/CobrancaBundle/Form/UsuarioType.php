<?php
namespace WZSistemas\CobrancaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of UsuarioType
 *
 * @author Luciano
 */
class UsuarioType extends AbstractType
{
    public function getName()
    {
        return "usuario";
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nome')
            ->add('email', "email")
            ->add('senha', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => "Insira a senha.",
                    'first_options'  => array('label' => 'Senha'),
                    'second_options' => array('label' => 'Confirmar senha'),
                ))
            ->add('nivel', 'entity', array(
                'class'         => 'WZSistemasCobrancaBundle:Nivel',
                'empty_value'   => 'Selecione',
                'empty_data'    => null,
                'label'         => 'Nível'
        ));

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WZSistemas\CobrancaBundle\Entity\Usuario',
        ));
    }

}
